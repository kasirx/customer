Aplikasi Kasir

Ini adalah aplikasi kasir yang memiliki dua fitur/dua project yaitu Customer dan Items.

di project ini fiturnya adalah customer, terdapat fungsi :

1. Menambahkan Data
2. Membaca Data
3. Mengubah Data
4. Menghapus Data

Keterangan database :
Nama database = kasir
Nama tabel = customers
host="localhost"

Di project ini menggunakan database Mysql, Flask, Connector dan keamanan JWT
Daftar Rest Api

1. /customers = Melihat semua data Customers
2. /customer = Melihat data customer sesuai id
3. /insert = untuk menambahkan data customer baru
4. /update = untuk merubah data customer
5. /delete = untuk menghapus data customer
6. /requesttoken = untuk mendapatkan token yang berasal dari data email customer
