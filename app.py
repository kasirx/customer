from controller import customers
from flask import Flask,jsonify,request
from flask_jwt_extended import JWTManager

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False
app.config['JWT_SECRET_KEY'] = "testkunci"
jwt = JWTManager(app)

@app.route("/customers", methods=["POST"])
def showCustomers():
    return customers.shows()

@app.route("/customer", methods=["POST"])
def showCustomersById():
    params = request.json
    return customers.showById(**params)

@app.route("/insert", methods=["POST"])
def insertCustomer():
    params = request.json
    return customers.insertCustomer(**params)

@app.route("/update", methods=["POST"])
def updateCustomer():
    
    params = request.json
    return customers.updateCustomer(**params)

@app.route("/delete", methods=["POST"])
def deleteCustomer():
    
    params = request.json
    return customers.deleteCustomer(**params)

@app.route("/requsttoken", methods=["POST"])
def requestToken():
    params = request.json
    return customers.token(**params)

if __name__ == "__main__":
    app.run(debug=True)
