from mysql.connector import connect

class database:
    def __init__(self):
        try:
            self.db = connect(host = "localhost",
                                user = "root",
                                password = "windioya25",
                                database = "kasir")
            if self.db.is_connected():
                print("Connected to MySQL database")
        except Exception as e:
            print(e)
    
    def showCustomers(self):
        try:
            cursor = self.db.cursor()
            query = '''select * from customers'''
            cursor.execute(query)
            result = cursor.fetchall()
            return result
        except Exception as e:
            print(e)
    
    def showCustomersById(self, **params):
        try:
            cursor = self.db.cursor()
            query = '''select * from customers where customer_id = {0};'''.format(params["customer_id"])
            cursor.execute(query)
            result = cursor.fetchone()
            return result
        except Exception as e:
            print(e)


    def showCustomersByEmail(self, **params):
        try:
            cursor = self.db.cursor()
            query = '''select * from customers where email = "{0}";'''.format(params["email"])
            cursor.execute(query)
            result = cursor.fetchone()
            return result
        except Exception as e:
            print(e)

    def insertCustomer(self, **params):
            try:
                column = ', '.join(list(params.keys()))
                values = tuple(list(params.values()))
                crud_query = '''insert into customers ({0}) values {1};'''.format(column, values)
                
                cursor = self.db.cursor()
                cursor.execute(crud_query)
            except Exception as e:
                print(e)
    
    def updateCustomerById(self, **params):
        try:
            customer_id = params['customer_id']
            values = self.restructureParams(**params['values'])
            crud_query = '''update customers set {0} where customer_id = {1};'''.format(values, customer_id)

            cursor = self.db.cursor()
            cursor.execute(crud_query)
        except Exception as e:
            print(e)
    
    def deleteCustomerById(self, **params):
        try:
            customer_id = params['customer_id']
            crud_query = '''delete from customers where customer_id = {0};'''.format(customer_id)

            cursor = self.db.cursor()
            cursor.execute(crud_query)
        except Exception as e:
            print(e)

    def dataCommit(self):
        self.db.commit()
        
    def restructureParams(self, **data):
        list_data = ['{0} = "{1}"'.format(item[0],item[1]) for item in data.items()]
        result = ', '.join(list_data)
        return result

    def closeConnection(self):
        if self.db is not None and self.db.is_connected():
            self.db.close()