from models.customers import database
from flask import Flask, jsonify, request
from flask_jwt_extended import *
import datetime

mysqldb = database()

@jwt_required()
def shows():
    dbresult = mysqldb.showCustomers()
    result = []
    print(dbresult)
    for items in dbresult:
        customer = {
            "customer_id" : items[0],
            "name" : items[1],
            "email" : items[2]
        }
        result.append(customer)
    return jsonify(result)

def showById(**params):
    dbresult = mysqldb.showCustomersById(**params)
    if dbresult is not None:
        customer = {
                "customer_id" : dbresult[0],
                "name" : dbresult[1],
                "email" : dbresult[2]
        }
    else:
        customer = {
            "message" : "Id Tidak Terdaftar"
        }
    return jsonify(customer)

def insertCustomer(**params):
    mysqldb.insertCustomer(**params)
    mysqldb.dataCommit()
    return ({"message":"Data Berhasil Di Masukkan"})

def updateCustomer(**params):
    mysqldb.updateCustomerById(**params)
    mysqldb.dataCommit()
    return ({"message":"Data Berhasil Diubah"})

def deleteCustomer(**params):
    mysqldb.deleteCustomerById(**params)
    mysqldb.dataCommit()
    return ({"message":"Data Berhasil Dihapus"})

def token(**params):
    dbresult = mysqldb.showCustomersByEmail(**params)
    if dbresult is not None:
        customer = {
            "name" : dbresult[1],
            "email" : dbresult[2]
        }
        expires = datetime.timedelta(days=1)
        expires_refresh = datetime.timedelta(days=3)
        access_token = create_access_token(customer, fresh=True, expires_delta=expires)

        data={
            "data" : customer,
            "token_access" : access_token
        }
    else:
        data = {
                "message":"Email Tidak Terdaftar"
            }
    return jsonify(data)